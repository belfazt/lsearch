/* 
 * File:   lsearch.c
 * Author: vagrant
 *
 * Created on February 11, 2015, 5:47 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <search.h>

/*
 * 
 */
char *names[] = {"Juan","Lucia","Pedro","Jesus","Maria","Antonio"};

int compare (const void * a, const void * b){
  return strcmp( *(char**)a, *(char**)b );
}


int main() {
  int *pointerToItem;
  char *key;
  printf("Escribe el nombre que quieres buscar: ");
  scanf("%s", key);
  size_t size = 6;
  pointerToItem = (int*)lfind(&key, &names, &size , sizeof(char *), compare);
  if (pointerToItem != NULL){
    printf ("La posición del arreglo donde se encuentra el nombre es: %d\n",((int)pointerToItem-(int)&names)/sizeof(char *));
    printf ("La dirección del memoria donde se encuentra el nombre es: %p\n", pointerToItem);
  }
  else{
    printf("No se encontró el nombre que buscabas\n");
  }
  return (EXIT_SUCCESS);
}

