#!/usr/bin/expect --

# For colors
proc capability cap {expr {![catch {exec tput -S << $cap}]}}
proc colorterm {} {expr {[capability setaf] && [capability setab]}}
proc tput args {exec tput -S << $args >/dev/tty}
array set color {black 0 red 1 green 2 yellow 3 blue 4 magenta 5 cyan 6 white 7}
proc foreground x {exec tput -S << "setaf $::color($x)" > /dev/tty}
proc background x {exec tput -S << "setab $::color($x)" > /dev/tty}
proc reset {} {exec tput sgr0 > /dev/tty}

#Test the program
eval spawn [lrange $argv 0 end]

#Use send to give input to the program
#use expect for the expected results

#As example, send 2 lines with three values each and expect 15 as result
send "Juan\r"
expect "0" {foreground green; puts "\nPASSED";reset} default {foreground red;puts "\nFAILED";reset}
